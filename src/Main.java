import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Выделил подзадачи в отдельные методы, чтобы было удобнее
        Scanner myScan = new Scanner(System.in);
        String str = myScan.nextLine();

        str = replaceIgnoreCase(str, "object-oriented programming", "OOP", 2);
        // System.out.println(str);

        String word = getWordWithMinCountUniqueSymbols(str);
        // System.out.println(word);

        int count = countLatinWords(str);
        // System.out.println(count);

        String [] palindromes = findPalindromes(str);
        /* for (String palindrome:palindromes)
            System.out.println(palindrome); */
    }

    public static String [] findPalindromes(String str) {
        String palindromes = "";
        for (String word : str.split(" ")) {
            int i = 0;
            int j = word.length() - 1;
            boolean isPalindrome = true;
            while (i < j) {
                if (word.charAt(i) != word.charAt(j)) {
                    isPalindrome = false;
                    break;
                }
                i++;
                j--;
            }
            if (isPalindrome)
                palindromes += (word + " ");
        }
        return palindromes.split(" ");
    }

    public static int countLatinWords(String str) {
        int count = 0;
        for (String word : str.split(" ")) {
            if (word.matches("[a-zA-z]+"))
                count++;
        }
        return count;
    }

    public static int countUniqueSymbols(String word) {
        int count = 0;
        boolean [] symbolsIncludes = new boolean[Character.MAX_VALUE];
        for (int i = 0; i < word.length(); i++) {
            symbolsIncludes[word.charAt(i)] = true;
        }
        for (boolean elem : symbolsIncludes) {
            if (elem)
                count++;
        }
        return count;
    }

    public static String getWordWithMinCountUniqueSymbols(String str) {
        String correctWord = str.split(" ")[0];
        int minCount = Integer.MAX_VALUE;
        for (String word : str.split(" ")) {
            int count = countUniqueSymbols(word);
            if (count < minCount) {
                correctWord = word;
                minCount = count;
            }
        }
        return correctWord;
    }

    public static String replaceIgnoreCase (String str, String oldString, String newString, int numberOfOccurrence) {
        int count = 0;
        int oldStringLength = oldString.length();
        int index = 0;
        while(true) {
            if (index > str.length() - oldStringLength)
                break;
            if (str.substring(index, index + oldStringLength).compareToIgnoreCase(oldString) == 0) {
                count++;
                if (count % numberOfOccurrence == 0)
                    str = str.substring(0, index) + newString + str.substring(index + oldStringLength);
            }
            index++;
        }
        return str;
    }
}
